﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using XOGameLogic;

namespace NewGameXO.WFLib
{
	/// <summary>
	/// Description of FigureO.
	/// </summary> 

	public abstract class AbstractWFGameFigure : IGameFigure
	{
		protected readonly int width;
		protected Pen currentPen;
		public AbstractWFGameFigure()
		{
		}
		public abstract void Display(Object obj, int x, int y);
	}
}
