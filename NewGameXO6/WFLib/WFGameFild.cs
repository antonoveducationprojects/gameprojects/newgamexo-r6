﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using XOGameLogic;

namespace NewGameXO
{
	/// <summary>
	/// Description of FigureO.
	/// </summary> 
	
	public class WFGameFild : AbstractGameFild
	{
		Pen currentPen;
		private readonly int width;
		public WFGameFild(IGameFigure X, IGameFigure O, int width=200):base(X, O)
		{
			this.width = width;
			currentPen= new Pen(Color.Black,2);

		}
		public override void Display(Object obj)
		{
			int figureCode;
			Graphics gDev = (Graphics)obj;
			for (int j=1;j<=2;++j)
		    {
				gDev.DrawLine(currentPen,1,j*width,3*width,j*width);
				gDev.DrawLine(currentPen,j*width,0,j*width,3*width);
			}
			//foreach(var e in figures)
   //         {
			//	System.Windows.Forms.MessageBox.Show(e.ToString());
			//}
			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
				{
					figureCode = GameMap[i, j];
					if (figureCode == CodeFree)
						continue;
					figures[figureCode].Display(gDev,i * width, j * width);
				}
		}

		public override void HihlightWinner(XOGameLogic.Point Start, XOGameLogic.Point End)
        {
			//throw new NotImplementedException();
        }
	}	
}
