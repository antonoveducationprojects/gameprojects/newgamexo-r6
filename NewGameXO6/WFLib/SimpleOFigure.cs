﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using XOGameLogic;

namespace NewGameXO.WFLib
{
	/// <summary>
	/// Description of FigureO.
	/// </summary> 
	public class SimpleOFigure : AbstractWFGameFigure
	{
		public SimpleOFigure() : base()
		{
			currentPen = new Pen(Color.Green, 8);
		}
		public override void Display(Object obj, int x, int y)
		{
			Graphics gDev = (Graphics)obj;
			//int offset = width / 20, offset2 = width - offset;
			gDev.DrawEllipse(currentPen, x + 10, y + 10, 180, 180);
		}
	}
}
