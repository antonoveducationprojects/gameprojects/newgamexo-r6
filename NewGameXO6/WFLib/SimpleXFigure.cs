﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;

namespace NewGameXO.WFLib
{
	public class SimpleXFigure : AbstractWFGameFigure
	{

		public SimpleXFigure():base()
		{
			currentPen= new Pen(Color.Black,8);
		}
		public override void Display(Object obj, int x, int y)
		{
			Graphics gDev = (Graphics)obj;
			int offset = width/20,offset2=width-offset;
			gDev.DrawLine(currentPen,x+10,y+10,x+190,y+190);
			gDev.DrawLine(currentPen,x+180,y+10,x+10,y+190);
			//gDev.DrawLine(currentPen,x+offset,y+offset,x+offset2,y+offset2);
			//gDev.DrawLine(currentPen,x+offset,y+offset,x+offset2,y+offset2);
		}

	}

}
