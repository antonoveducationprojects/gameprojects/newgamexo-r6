﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using XOGameLogic;

namespace NewGameXO.WFLib
{
    public class WFGameFactory : AbstractGameFactory
    {
        public WFGameFactory()
        {
        }

        public override AbstractGameFild CreateGameFild()
        {
             
            return new WFGameFild(CreateXFigure(), CreateOFigure());//-+
        }

        public override IGameFigure CreateOFigure()
        {
            return new SimpleOFigure();
        }

        public override IGameFigure CreateXFigure()
        {
            return new SimpleXFigure();
        }
    }
}
