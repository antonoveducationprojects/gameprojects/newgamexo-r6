﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:18
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace NewGameXO
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.PictureBox paintArea;
		private System.Windows.Forms.Button ResetButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label stepLabel;
		private System.Windows.Forms.Label msgLabel;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
            this.paintArea = new System.Windows.Forms.PictureBox();
            this.ResetButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.stepLabel = new System.Windows.Forms.Label();
            this.msgLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.paintArea)).BeginInit();
            this.SuspendLayout();
            // 
            // paintArea
            // 
            this.paintArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.paintArea.Location = new System.Drawing.Point(14, 8);
            this.paintArea.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.paintArea.MaximumSize = new System.Drawing.Size(600, 600);
            this.paintArea.MinimumSize = new System.Drawing.Size(600, 600);
            this.paintArea.Name = "paintArea";
            this.paintArea.Size = new System.Drawing.Size(600, 600);
            this.paintArea.TabIndex = 0;
            this.paintArea.TabStop = false;
            this.paintArea.Paint += new System.Windows.Forms.PaintEventHandler(this.PaintAreaPaint);
            this.paintArea.MouseClick += new System.Windows.Forms.MouseEventHandler(this.PaintAreaMouseClick);
            // 
            // ResetButton
            // 
            this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ResetButton.Location = new System.Drawing.Point(539, 708);
            this.ResetButton.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(88, 27);
            this.ResetButton.TabIndex = 1;
            this.ResetButton.Text = "RESET";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButtonClick);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(14, 707);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 27);
            this.label1.TabIndex = 2;
            this.label1.Text = "Current step:";
            // 
            // stepLabel
            // 
            this.stepLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.stepLabel.Location = new System.Drawing.Point(138, 707);
            this.stepLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.stepLabel.Name = "stepLabel";
            this.stepLabel.Size = new System.Drawing.Size(40, 27);
            this.stepLabel.TabIndex = 3;
            this.stepLabel.Text = "0";
            // 
            // msgLabel
            // 
            this.msgLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.msgLabel.Location = new System.Drawing.Point(184, 707);
            this.msgLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.msgLabel.Name = "msgLabel";
            this.msgLabel.Size = new System.Drawing.Size(313, 27);
            this.msgLabel.TabIndex = 4;
            this.msgLabel.Text = "in Process";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 763);
            this.Controls.Add(this.msgLabel);
            this.Controls.Add(this.stepLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.paintArea);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.MaximumSize = new System.Drawing.Size(744, 802);
            this.MinimumSize = new System.Drawing.Size(744, 802);
            this.Name = "MainForm";
            this.Text = "NewGameXO";
            ((System.ComponentModel.ISupportInitialize)(this.paintArea)).EndInit();
            this.ResumeLayout(false);

		}

		}
	}