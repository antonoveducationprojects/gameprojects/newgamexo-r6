﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XOGameLogic
{
    public interface IGameFigure
    {
        void Display(Object obj, int x, int y);
    }
}
