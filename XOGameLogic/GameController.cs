﻿using System;

namespace XOGameLogic
{
	public enum GameState//+
	{
		inProgress,
		Winner,
		GameOver
	}

	public class GameController
	{
		//+
		public int currentStep { get; private set; }//+
		public GameState gameState { get; private set; }//+
		public int WinnerCode { get; private set; }//+
		public Point[] Direction { get; private set; }//+
		protected AbstractGameFild gameFild;//---



		public GameController(/*IGameFigure Xfigure, IGameFigure OFigure, */AbstractGameFild gameFild)//???
		{
			gameState = GameState.inProgress;//+
			currentStep = 0;//+
			this.gameFild = gameFild;//?
			this.gameFild.ResetMap();//+
			Direction = new Point[2];//+
		}


		private void SetDirection(int x1, int y1, int x2, int y2)//+
		{
/*
            Direction[0].X = rectCenter + x1 * rectWidth;
			Direction[0].Y = rectCenter + y1 * rectWidth;
			Direction[1].X = rectCenter + x2 * rectWidth;
			Direction[1].Y = rectCenter + y2 * rectWidth;
*/
		}

		public void MakeJob(int i, int j)//???
		{
			if (gameState == GameState.GameOver)
				return;
			if (gameFild[i, j] != AbstractGameFild.CodeFree)
				return;
			gameFild[i, j] = currentStep % 2;
			currentStep++;
		}

		public bool ChangeStateToWinner(int j)//+
		{
			gameState = GameState.Winner;
			WinnerCode = j;
			return true;
		}

		public bool CheckWinner()//+ logic   - size
		{
			int[] checkRow, checkCol, checkDiag, checkDiag2;
			checkRow = new int[2];
			checkCol = new int[2];
			checkDiag = new int[2];
			checkDiag2 = new int[2];
			for (int i = 0; i < 3; ++i)
			{
				checkRow[0] = checkRow[1] = checkCol[0] = checkCol[1] = 0;
				for (int j = 0; j < 3; ++j)
				{
					if (-1 != gameFild[i, j])
						checkRow[gameFild[i, j]]++;
					if (-1 != gameFild[j, i])
						checkCol[gameFild[j, i]]++;
				}
				for (int j = 0; j < 2; ++j)
				{
					if (3 == checkRow[j])
					{
						SetDirection(i, 0, i, 2);
						return ChangeStateToWinner(j);
					}
					if (3 == checkCol[j])
					{
						SetDirection(0, i, 2, i);
						return ChangeStateToWinner(j);
					}
				}
				if (AbstractGameFild.CodeFree != gameFild[i, i])
					checkDiag[gameFild[i, i]]++;
				if (AbstractGameFild.CodeFree != gameFild[2 - i, i])
					checkDiag2[gameFild[2 - i, i]]++;
			}
			for (int j = 0; j < 2; ++j)
			{
				if (3 == checkDiag[j])
				{
					SetDirection(0, 0, 2, 2);
					return ChangeStateToWinner(j);
				}
				if (3 == checkDiag2[j])
				{
					SetDirection(2, 0, 0, 2);
					return ChangeStateToWinner(j);
				}
			}
			if (9 == currentStep)
			{
				gameState = GameState.GameOver;
				return true;
			}
			return false;
		}
		public void ResetGame()//+
		{
			gameFild.ResetMap();
			WinnerCode = -1;
			gameState = GameState.inProgress;
			currentStep = 0;
		}
		public AbstractGameFild GameFild { get => gameFild; }
	}
}
