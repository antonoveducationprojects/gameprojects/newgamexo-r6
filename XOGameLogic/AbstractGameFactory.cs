﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XOGameLogic
{
    public abstract class AbstractGameFactory
    {
        public abstract IGameFigure CreateXFigure();
        public abstract IGameFigure CreateOFigure();
        public abstract AbstractGameFild CreateGameFild();
    }
}
