﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 26.11.2018
 * Time: 18:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using XOGameLogic;

namespace XOGameLogic
{
	/// <summary>
	/// Description of FigureO.
	/// </summary> 

	public abstract class AbstractGameFild : IGameFild
	{
		protected int[,] GameMap = new int[3, 3];
		public const int CodeFree = -1;//+
		public const int CodeO = 0;//+
		public const int CodeX = 1;
		protected IGameFigure[] figures;//???
		public AbstractGameFild(IGameFigure X, IGameFigure O)
		{
			this.figures = new IGameFigure[2] { O, X };
		}
		public abstract void Display(Object obj);

		public abstract void HihlightWinner(XOGameLogic.Point Start, XOGameLogic.Point End);

		public void ResetMap()//+
		{
			for (int i = 0; i < 3; ++i)
				for (int j = 0; j < 3; ++j)
					GameMap[i, j] = CodeFree;
		}
		public int this[int i, int j]
        {
			get=>GameMap[i, j];
			set=> GameMap[i, j]=value;
		}

		public IGameFigure[] Figures { get => figures; }
	}
}
